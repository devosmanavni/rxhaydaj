//
//  CustomCell.swift
//  RxHayda
//
//  Created by Rooster on 2.03.2019.
//  Copyright © 2019 Rooster. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        print("Generic Cell Initialization Done")
    }
    
    func setupView () {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
}
