//
//  DetailViews.swift
//  RxHayda
//
//  Created by Rooster on 3.03.2019.
//  Copyright © 2019 Rooster. All rights reserved.
//

import UIKit

class DetailViews: UIView {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var subInfo1Label: UILabel!
    @IBOutlet weak var subInfo2Label: UILabel!
    @IBOutlet weak var subInfo3label: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

}
