//
//  ViewController.swift
//  RxHayda
//
//  Created by Rooster on 1.03.2019.
//  Copyright © 2019 Rooster. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewController: UIViewController {
    private let disposeBag = DisposeBag()
    private let throttleInterval = 0.1
    let customCellIdentifier = "SearchCell"
    let osmann = "osman as jkdnaskdnas"

    var repoViewModel:ViewModel?
    var apiProvider = Api()
    
    // Initialization clouse
    
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = 70
        tableView.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: customCellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    lazy var searchBar : MainSearchBar = {
        let searchBar = MainSearchBar()
        searchBar.returnKeyType = .done
        definesPresentationContext = true
        return searchBar
    }()
    
//    lazy var rightBarButton: UIBarButtonItem = {
////        let rightBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem, target:self, action:#selector(rightBarbuttonAction))
//        
//        
//    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activitiyIndicator = UIActivityIndicatorView()
        return activitiyIndicator
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        repoViewModel = ViewModel(api: self.apiProvider)
        
        if let viewModel = repoViewModel {
            
            viewModel.data.drive(tableView.rx.items(cellIdentifier: customCellIdentifier, cellType: SearchTableViewCell.self)) {
                row , osman, cell in
                    cell.nameLabel?.text = osman.name
                    cell.infoLabel?.text = osman.name
                    cell.setReaded(readed: false)
                    cell.itemImageView?.image = UIImage(named: "osman")
               
                }.disposed(by: disposeBag)
            
            tableView
                .rx // 1
                .modelSelected(Repistory.self) // 2
                .subscribe(onNext: { [weak self] repo in // 3
                   
                    self?.navigationController?.pushViewController(DetailViewController(), animated: false)
                    if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow { // 5
                        self?.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
                    }
                })
                .disposed(by: disposeBag) // 6
                        searchBar.rx.text.orEmpty.bind(to: viewModel.searchText).disposed(by: disposeBag)
        }
        
        
        setupView()
    }
    
    fileprivate func setupView(){
        view.addSubview(searchBar)
        view.addSubview(tableView)
        
        let contraints = [

            searchBar.topAnchor.constraint(equalTo: view.topAnchor,constant:60),
            searchBar.heightAnchor.constraint(equalToConstant: 70),
            searchBar.leftAnchor.constraint(equalTo: view.leftAnchor),
            searchBar.rightAnchor.constraint(equalTo: view.rightAnchor),
            
            
            tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
            
        ]
        NSLayoutConstraint.activate(contraints)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]
    }
}


struct Repistory {
    let name:String
    let url:String
}

struct ViewModel {
    
    var searchText = Variable("")
    let disposeBag = DisposeBag()
    
    let apiProvider:Api
    var data:Driver<[Repistory]>
    
    init(api:Api) {
        
        self.apiProvider = api
        
        data = searchText.asObservable()
            .throttle(0.3, scheduler: MainScheduler.instance)
            .filter({$0.count > 3})
            .distinctUntilChanged()
            .flatMapLatest {
                api.getRepistories($0)
            }.asDriver(onErrorJustReturn: [])
    }
}

class Api {
    
    func getRepistories(_ githubId:String) -> Observable<[Repistory]> {
        
        guard !githubId.isEmpty, let url = URL(string: "https://api.github.com/users/\(githubId)/repos") else {return Observable.just([])}
        
        return URLSession.shared
            .rx.json(request: URLRequest(url: url))
            .retry(1)
            .map {
                var repistories = [Repistory]()
                if let items = $0 as? [[String:AnyObject]] {
                    items.forEach({ (data) in
                        guard let name = data["name"] as? String,
                            let url = data["html_url"] as? String else {return}
                        
                        repistories.append(Repistory(name: name, url: url))
                    })
                    
                }
                
                return repistories
        }
    }
}
