//
//  DetailViewController.swift
//  RxHayda
//
//  Created by Rooster on 2.03.2019.
//  Copyright © 2019 Rooster. All rights reserved.
//

import UIKit
import RxSwift

class DetailViewController: UIViewController {

    private let selectedCharacterVeriable = Variable("User")
    var selectedCharacter: Observable <String> {
        return selectedCharacterVeriable.asObservable()
    }
    
    lazy var detailView: DetailViews = {
        let  detailView = DetailViews()
        return detailView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(detailView)
        selectedCharacterVeriable.value = "Osman"
        
    }
    
    func detailViewSetup() {
        detailView.backgroundColor = UIColor.red
        detailView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        detailView.collectionNameLabel?.text = "skmaskcmaskcakcas smcvskcms skmcvskd"
        detailView.itemImageView?.image = UIImage(named: "osman")
        detailView.subInfo1Label?.text  = " asmfasmasmfas"
        let contraints = [
            detailView.topAnchor.constraint(equalTo: view.bottomAnchor),
            detailView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            detailView.leftAnchor.constraint(equalTo: view.leftAnchor),
            detailView.rightAnchor.constraint(equalTo: view.rightAnchor),
            detailView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ]
        NSLayoutConstraint.activate(contraints)
    }

}
