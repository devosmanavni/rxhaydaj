//
//  MainSearchbar.swift
//  RxHayda
//
//  Created by Rooster on 2.03.2019.
//  Copyright © 2019 Rooster. All rights reserved.
//

import UIKit

class MainSearchBar: UISearchBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.placeholder = "Lutfen"
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
